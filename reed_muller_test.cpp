#include "reed_muller_test.h"
#include "helper.h"
#include <algorithm>
#include <unordered_map>

namespace Library {

	ReedMullerTest::ReedMullerTest() = default;
	ReedMullerTest::~ReedMullerTest() = default;
	
	std::vector<uint64_t> ReedMullerTest::synthesize(const kitty::dynamic_truth_table &t) {
		std::vector<uint64_t> char_function = char_from_truth_table(t);
		std::vector<uint64_t> result_function = char_from_function(char_function);
        //std::shared_ptr<GNet> result_net_pointer = funcToGNet(result_function);
		return result_function;
	}
	
	//the last cell in func has the number of variables the func depends on as the "coef" field
	uint64_t ReedMullerTest::apply(std::vector<uint64_t> &func, const std::string &s) {
		std::string s_ = s;
		std::reverse(s_.begin(), s_.end());
		std::vector<int> positions_of_ones;
		uint64_t num_var = func[func.size() - 1];
		uint64_t res = 0;
		
		for(int i = 0; i < s_.size(); ++i){
			if(s_[i] == '1') positions_of_ones.push_back(i);
		}
		uint64_t sz = positions_of_ones.size();
		uint64_t base = 1 << sz;
		for(int i = 0; i < base; ++i){
			std::string str = Helper::to2(i, sz);
			int pos = 0;
			for(uint64_t j = 0; j < sz; ++j){
				if(str[j] == '1') pos += 1 << (positions_of_ones[sz - 1 - j]);
			}
			res ^= func[pos];
		}
		return res;
	}
	/*
	void ReedMullerTest::polynomical(std::vector<Cell> func) {
		if(func.size() == 2 && func[0].coef == 0){
			std::cout << "0";
			return;
		}
		for(int i = 0; i < func.size()-1; ++i){
			if(i == 0 && func[0].variables.empty()){
				std::cout << "1 ";
			}
			for(int variable : func[i].variables){
				std::cout << "x_" << variable + 1 << ' ';
			}
			
			if(i != func.size() - 2)std::cout << "^ ";
			
		}
	}*/
	
	std::vector<uint64_t> ReedMullerTest::char_from_truth_table(const kitty::dynamic_truth_table &t) {
		std::vector<uint64_t> char_function;
		uint64_t num_bits = t.num_bits();
		uint64_t num_var = t.num_vars();
		char_function.resize(num_bits + 1);
		for(int i = 0; i < num_bits; ++i){
			char_function[i] = kitty::get_bit(t, i);
		}
		char_function[char_function.size() - 1] = num_var;
		return char_function;
	}
	
	std::vector<uint64_t> ReedMullerTest::char_from_function(std::vector<uint64_t> &func) {
		uint64_t num_var = func[func.size()-1];
		uint64_t num_bits = 1 << num_var;
		std::vector<uint64_t> result_function(num_bits + 1);
		for(int i = 0; i < num_bits; ++i){
			result_function[i] = apply(func, Helper::to2(i, num_var));
		}
		result_function[result_function.size()-1] = num_var;
		return result_function;
	}

    std::shared_ptr<GNet> ReedMullerTest::funcToGNet(std::vector<uint64_t> &func) {
        bool flag = false;
        Gate::SignalList inputs;
        Gate::Id outputId;
        uint64_t n = func[func.size() - 1];
        auto net = std::make_shared<GNet>();
        std::vector<Gate::Id> var_id(n);
        Gate::SignalList output;
        for (int i = 0; i < n; ++i){
            var_id[i] = net->addIn();
            inputs.push_back(Gate::Signal::always(var_id[i]));
        }
        if(func[0] == 1) flag = true;
        for(int i = 1; i < func.size() - 1; ++i){
            if(func[i] == 0) continue;
            std::vector<int> variables = Helper::popcnt(i);
            Gate::SignalList cur_vars;
            for(int variable : variables) cur_vars.push_back(inputs[variable]);
            Gate::Id id;
            if(flag) {
                id = net->addGate(GateSymbol::NAND, cur_vars);
                flag = false;
            }
            else  id = net->addGate(GateSymbol::AND, cur_vars);
            output.emplace_back(Gate::Signal::always(id));
        }
        Gate::Id gateId;
        if(!output.empty()) gateId = net->addXor(output);
        else if (func[0] == 1) gateId = net->addOne();
        else gateId = net->addZero();
        outputId = net->addOut(gateId);
        net->sortTopologically();
        return net;
    }
	
}
