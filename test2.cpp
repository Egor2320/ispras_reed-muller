#include <gtest/gtest.h>
#include "gmock/gmock.h"

#include "reed_muller_test.h"
#include "helper.h"

using testing::Eq;
namespace {
	class ClassDeclaration : public testing::Test {
		public:
		Library::ReedMullerTest r;
		
		ClassDeclaration() {
			r;
		}
		
	};
}

TEST_F(ClassDeclaration, nameOfTheTest3){
	kitty::dynamic_truth_table t(3);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<3));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest4){
	kitty::dynamic_truth_table t(4);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<4));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest5){
	kitty::dynamic_truth_table t(5);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<5));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest6){
	kitty::dynamic_truth_table t(6);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<6));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest7){
	kitty::dynamic_truth_table t(7);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<7));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest8){
	kitty::dynamic_truth_table t(8);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<8));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest9){
	kitty::dynamic_truth_table t(9);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<9));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest10){
	kitty::dynamic_truth_table t(10);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<10));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest11){
	kitty::dynamic_truth_table t(11);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<11));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest12){
	kitty::dynamic_truth_table t(12);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<12));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest13){
	kitty::dynamic_truth_table t(13);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<13));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest14){
	kitty::dynamic_truth_table t(14);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<14));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest15){
	kitty::dynamic_truth_table t(15);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<15));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest16){
	kitty::dynamic_truth_table t(16);
	kitty::create_from_binary_string(t, Library::Helper::generate(1<<16));
	r.synthesize(t);
}

TEST_F(ClassDeclaration, nameOfTheTest20){
    kitty::dynamic_truth_table t(20);
    kitty::create_from_binary_string(t, Library::Helper::generate(1<<20));
    r.synthesize(t);
}




