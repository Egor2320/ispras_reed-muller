#include "kitty-master/include/kitty/kitty.hpp"
#include "gate/model/gnet.h"
#include <iostream>
#include <memory>
#include <unordered_map>
#include <unordered_set>

#ifndef REED_MULLER_TEST_LOGICAL_FUNCTION_H
#define REED_MULLER_TEST_LOGICAL_FUNCTION_H
namespace Library {
    using Gate = eda::gate::model::Gate;
    using GNet = eda::gate::model::GNet;
    using GateSymbol = eda::gate::model::GateSymbol;
	
	/**
	* Class LogicalFunction creates a polynomial P(x_1, x_2, ... , x_n) = a_0 ^ (a_1 & x_1) ^ (a_2 & x_2) ^ ... ^ (a_12 & x_1 & x_2) ^ ... by a given truth table
	*/
	class ReedMullerTest {
		public:
		/**
		 * Default constructor
		 */
		ReedMullerTest();
		
		/**
		 * Default virtual destructor
		 */
		virtual ~ReedMullerTest();
		
		/**
		 * Creates a function, represented by a given truth table \n
		 * The result function is stored as a std::vector<Cell> \n
		 * Sample output: \n kitty:create_from_binary_string(TT t, "10011100");\n
		 * 								synthesize(t) = x_2 ^ x_3 ^ x_1 & x_3
		 *
		 * @return std::vector<Cell> func
		 */
		std::vector<uint64_t> synthesize(const kitty::dynamic_truth_table& t);
        std::shared_ptr<GNet> funcToGNet (std::vector<uint64_t> &func);
		
		/**
		* Applies the given function to a given binary string \n
		* Before applying the function checks whether the size of a given binary string is right (is the same, as the number of variables)\n
		* If s.size() < num_vars -> add leading zeroes to make the padding right \n
		* If s.size() > num_vars -> throw an InvalidPadding excepton (it's a custom exception of class LogicalFunction)
		*
		* @return uint64_t ans
		*/
		uint64_t apply(std::vector<uint64_t> &func, const std::string &s);
		
		private:
		std::vector<uint64_t> char_from_truth_table(const kitty::dynamic_truth_table &t);
		std::vector<uint64_t> char_from_function (std::vector<uint64_t> &func);
		
		
		class InvalidPadding : public std::exception{
			public:
			InvalidPadding(const char *message) : _description(message) {};
			const char * what() const noexcept override {
				return _description;
			}
			private:
			const char * _description;
		};
		
	};
}

#endif //REED_MULLER_TEST_LOGICAL_FUNCTION_H
