#include <gtest/gtest.h>
#include "gmock/gmock.h"

#include "reed_muller_test.h"
#include "helper.h"

using testing::Eq;
namespace {
	class ClassDeclaration : public testing::Test {
		public:
		Library::ReedMullerTest r;
		
		ClassDeclaration() {
			r;
		}
		
	};
}

//Testing if synthesis function works correctly
//We generate a random binary string of length 2^6, 2^10 and 2^14 respectively
TEST_F(ClassDeclaration, nameOfTheTest1){
	uint64_t size = 6;
	uint64_t bits = 1 << size;
	kitty::dynamic_truth_table t(size);
	std::string s = Library::Helper::generate(bits);
	kitty::create_from_binary_string(t,s);
	std::vector<uint64_t> f = r.synthesize(t);
	for(int i = 0; i < bits; ++i){
		ASSERT_EQ(r.apply(f, Library::Helper::to2(i, size)), kitty::get_bit(t, i));
	}
	
}

TEST_F(ClassDeclaration, nameOfTheTest2){
	uint64_t size = 10;
	uint64_t bits = 1 << size;
	kitty::dynamic_truth_table t(size);
	std::string s = Library::Helper::generate(bits);
	kitty::create_from_binary_string(t,s);
	std::vector<uint64_t> f = r.synthesize(t);
	for(int i = 0; i < bits; ++i){
		ASSERT_EQ(r.apply(f, Library::Helper::to2(i, size)), kitty::get_bit(t, i));
	}
	
}

TEST_F(ClassDeclaration, nameOfTheTest3){
	uint64_t size = 14;
	uint64_t bits = 1 << size;
	kitty::dynamic_truth_table t(size);
	std::string s = Library::Helper::generate(bits);
	kitty::create_from_binary_string(t,s);
	std::vector<uint64_t> f = r.synthesize(t);
	for(int i = 0; i < bits; ++i){
		ASSERT_EQ(r.apply(f, Library::Helper::to2(i, size)), kitty::get_bit(t, i));
	}
}

//see if the "00000000" on 3 variables synthesizes a correct function
TEST_F(ClassDeclaration, nameOfTheTest4){
	kitty::dynamic_truth_table t(3);
	kitty::create_from_binary_string(t, "00000000");
	std::vector<uint64_t> func(9);
	func[8] = 3;
	func[0] = 0;
	func[1] = 0;
	func[2] = 0;
	func[3] = 0;
	func[4] = 0;
	func[5] = 0;
	func[6] = 0;
	func[7] = 0;
	std::vector<uint64_t> ans = r.synthesize(t);
	//should be an empty vector with ans[0] == 3 (num_var);
	ASSERT_EQ(func, ans);
}

//see if the "11111111" on 3 variables synthesizes a correct function
TEST_F(ClassDeclaration, nameOfTheTest5){
	kitty::dynamic_truth_table t(3);
	kitty::create_from_binary_string(t, "11111111");
	std::vector<uint64_t> func;
	func.resize(9);
	func[8] = 3;
	func[0] = 1;
	func[1] = 0;
	func[2] = 0;
	func[3] = 0;
	func[4] = 0;
	func[5] = 0;
	func[6] = 0;
	func[7] = 0;
	std::vector<uint64_t> ans = r.synthesize(t);
	//should be a vector with ans[1] = 3 and ans[0] = 1;
	ASSERT_EQ(func, ans);
}

//compare the time of synthesis of the function of different number of variables
